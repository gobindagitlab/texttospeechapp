package com.example.texttospeech;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

public
class MainActivity extends AppCompatActivity {
  android.speech.tts.TextToSpeech textToSpeech;
    private android.widget.EditText editText;
    private android.widget.Button speek_btn;

    @Override
    protected
    void onCreate ( Bundle savedInstanceState ) {
        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.activity_main );


        editText=findViewById ( com.example.texttospeech.R.id.editText_speeech );
        speek_btn=findViewById ( com.example.texttospeech.R.id.button_speechout );
        textToSpeech=new android.speech.tts.TextToSpeech ( getApplicationContext ( ) , new android.speech.tts.TextToSpeech.OnInitListener ( ) {
            @Override
            public
            void onInit ( int status ) {
                if ( status != android.speech.tts.TextToSpeech.ERROR ){
                    textToSpeech.setLanguage ( java.util.Locale.UK );

                }
            }
        });

        speek_btn.setOnClickListener ( new android.view.View.OnClickListener ( ) {
            @Override
            public
            void onClick ( android.view.View v ) {
              String speak= editText.getText ().toString ();
                android.widget.Toast.makeText ( getApplicationContext (), speak, android.widget.Toast.LENGTH_SHORT ).show ();
                textToSpeech.speak ( speak, android.speech.tts.TextToSpeech.QUEUE_FLUSH,null );

            }
        } );

    }
    public void onPause(){
        if ( textToSpeech!=null ){
            textToSpeech.stop ();
            textToSpeech.shutdown ();
        }
        super.onPause ();

    }




}
